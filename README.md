# Minestack Library

Minestack is a library for Computercraft turtles, adding a command stack and a regular language to simplify movement instructions and reversal

## Introduction

Minestack uses a compact string representation for turtle movement commands. It supports reversing commands by storing all movement commands in a stack. Convenience functions for non-library functions like 'dig right' exist. Post and Pre movement steps can be defined via the API using the same language.



## Language Reference
The movement language is defined as follows

### Syntax

Movement ::= {Single_movement}

Single_movement ::= [Repeat][Action][Slot]Direction

Repeat ::= **Decimal number**  
Action ::= **e** | **s **| **p**  
Slot ::= **1-9**  
Direction ::= **f** | **b** | **u** | **d** | **l** | **r** | **n**

### Movement

The direction constants correspond to the movement of the turtle as follows. 

f - forward  
b - back  
u - up  
d - down  
l - turn left  
r - right  
n - No operation 

When no action is given within a single_movement, these constants will immediately result into a corresponding movement. If written as uppercase letters, the single_movement will execute both pre and post-movement steps, regardless wether an action is prepended or not. The "n" movement itself does not cause any motion and does not support prepended action constants, but "N" will still execute pre/post-steps.

The action constants correspond to following turtle-actions:

e - Dig ("Excavate")  
s - Detect ("Scan")  
p - Place  

Action constants will not move the turtle into the appended direction, but rather execute the action into that direction. For convenience, non-library actions have been defined in the minestack library, thus "er" will dig the block to the right as expected. The Place-action is the only one using the slot-parameter, when defined it will try to place a block from the corresponding slot of the turtle, if undefined it will place the next available block inside the turtles inventory.

When a repeat count is defined, then the following movement/action is executed accordingly. 

### Examples

    20F20b
Move 20 blocks forward, executing pre/post steps, and move 20 blocks backwards without pre/post steps




## API Reference

### Minestack Global functions
All global functions are called from the minestack namespace
#### *minestack.new*
Creates a new minestack object, which is the base of all minestack operations. The command stack itself is accessible via the numeric indexes of the object e.g. `myStack[1]` always contains the bottom of the stack and `myStack[#myStack]` contains the top. 

    minestack.new()
##### Returns
A new minestack object. 

### Minestack Member variables
All minestack member variables can be set directly to a minestack object, e.g. `myStack.pre="pdpr"`
#### *pre*
Defines the pre-movement step that is executed before each movement/action that has pre/post - steps enabled.
The behavior depends on the type of the value that is assigned:
When set to a string written in the minestack movement language, this string is executed as if it was written in lowercase(thus no prepost recursion occurs).   
When set to a lua function, the function is called with the minestack object as parameter.   
When set to a array-table, containing either movement language strings or functions, the string/function within the index of the current prepost-recursion level is executed.

#### *post*
Defines the pre-movement step that is executed after each movement/action that has pre/post - steps enabled.
The behavior depends on the type of the value that is assigned:
When set to a string written in the minestack movement language, this string is executed as if it was written in lowercase(thus no prepost recursion occurs).   
When set to a lua function, the function is called with the minestack object as parameter.   
When set to a array-table, containing either movement language strings or functions, the string/function within the index of the current prepost-recursion level is executed.
#### *lazyDig*
If lazyDig is set to true then excavate steps always report as succesful, thus never will abort a command string.
#### *forceDig*
If forceDig is set to true, then excavate steps will automatically repeat itself until no more block can be mined (because either no more block exists, or the block cannot be mined). A forceDig excavate will always return false, thus it is **strongly** recommended to use lazyDig.

*Warning:* ForceDig can lead to infinite loops within flowing liquids, as both detector and dig-steps return positive results as soon as the liquids regenerates, consider moveDig as safer alternative .

#### *movedig*
if moveDig is set to true, failed movement steps will try to excavate the obstacle until movement is successful or the "moveDigLimit" attempts are reached. This method is recommended for excavating in low depths, as spontaneous cobble-stone generation due to lava-water collision can confuse forceDig - steps, also moveDig cannot be confused by liquids.  

#### *moveDigLimit*
Sets the maximum number of movedig steps before abortion. Default is set to 32.

#### *x,y,z,dx,dy*
The current local coordinates and orientation of the turtle. The origin is the position of the turtle at the time of creation of the minestack object. It is not recommended to change these values manually. By default, the turtle towards the positive x-direction. 

### Minestack Member functions
All minestack member functions are called from a minestack object (see `minestack.new()`), using the colon operator.  
Example:

    myStack = minestack.new()
    myStack:execString("20F20b")

All single_movements without repeat and slot numbers can be called as directly member functions. Slot numbers can be placed as parameters for placing functions. 
Example:
    
    myStack:f() -- Moves one block forward
    myStack:pd() -- Places a block down
    myStack:pu(2) -- Places a block up from slot 2
    
All movement/actions issues via the minestack-object are pushed onto the stack (see Issues for exceptions)

#### *push*
Pushes a value onto the stack. Note that no sanity check is done on the value, thus care is required when pushing foreign values.

    push(value)
##### Parameters
###### `value`
Value to be pushed onto the stack

#### *pop*
Pops a value from the stack and returns it

    pop()
##### Returns
The top-value of the stack
####


#### *clear*
Clears the stack.

    clear()

#### *reverse*
Reverses the movement/action at the top of the stack

    reverse()
##### Returns
True if reveral was succesful, false otherwise

#### *reversePop*
Reverses and pops the movement/action at the top of the stack. If the movement/action cannot be undone, the stack is not modified

    reversePop()
##### Returns
True if reversal was succesful, false otherwise

#### *reversePopAll*
Reverses and pops the entire stack if possible

    reversePopAll()
##### Returns
True if reversal was succesful, false otherwise

#### *goto*
Moves the turtle to given local position. The turtle will face the x-direction afterwards. 

    goto(x,y,z)
##### Parameters
###### `x,y,z`
Local coordinates
##### Returns
True if succesful, false, error message and error word otherwise
#### *execString*
Executes a string interpreted as movement language command. For convenience, the call-operator for minestack-object is overloaded with execString, thus `myStack("20FrF19b")` has the same effect as calling execString.

    execString(command)
##### Parameters
###### `command`
A string written in the minestack movement language
##### Returns
True if succesful, false, error message and error word otherwise

## Examples

### Basics
6 Block hollow square, built with block in slot2 :   

    myStack.post="p2d"
    myStack("u 6Fr 6Fr 6Fr 6Fr") -- the lowercase 'r' prevents prepost-steps to be executed on already visited blocks
    

### Advanced
Wall using recursive prepost steps:   

    myStack.pre = {"10F10b","pl"}
    myStack("6U")

Before every 'U' step, the string "10F10b" is executed, thus moving forth 10 blocks and returning afterwards. Before every 'F' step during the first order pre-step, "pl" is executed, thus placing a block to the left of the turtle. 

Equivalent solution:  

    myStack.pre = {"6U6d","pl"}
    myStack("10F")


8 Block hollow tower, 12 blocks high, no floors/ceiling:  

    myStack.post={"u4N", "8Fr", "p1d"}
    myStack("12N")
    
The No-Op steps here are used as convenient placeholders for loops, while they do not do anything themselves they still trigger post-steps when written in Uppercase-letters. In this example, all N-steps in "12N" trigger an "u4N" word. All N-Steps in this first post-string thus trigger the "8Fr" four times, causing a hollow square movement(see first basic example). Finally, all F-steps in "8Fr" trigger "p1d" to place a block below the turtle from slot 1  if possible.

3 times 6-square flooring, each 4 blocks, from top to down:  

    myStack.post={"N4d",minestackLib.square(6).."rr","p1u"}
    myStack("3N")



## Issues
Not all commands are put into the stack yet due to lack off reversal commands  
Scan operation not implemented yet  
reversalPop should return both true and the former top value of the stack if succesful  
Pre/Post steps can cause infinite recursion when uppercase letters are used within their definition  
execString should return false and word and word-number when unsuccesful  

##Future
Changeable pre/post steps as language feature like `!Post(pdpr)20F!Post(pu)20B`  
User callback for detect operations  


















